package com.epam.home;

import com.epam.home.view.MyView;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException {
        new MyView().show();
    }

}
