package com.epam.home.controller;

import com.epam.home.model.Commodity;

import java.io.IOException;
import java.util.List;

public interface Controller {

    void printCommodities(List<Commodity> commodities);

    void getAllCommodities();

    void readCommodities() throws IOException;

    Commodity findByName(String name);

    List<Commodity> findByPrice(Character filter, double price);

    List<Commodity> findByGroupName(String groupName);

    List<Commodity> findByDepartment(String departmentName);

}
