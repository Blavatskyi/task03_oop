package com.epam.home.controller;

import com.epam.home.model.Commodity;

import java.io.IOException;
import java.util.List;

public class ControllerImpl implements Controller {
    Commodity commodity;

    public ControllerImpl() {
        commodity = new Commodity();
    }

    @Override
    public List<Commodity> findByDepartment(String departmentName) {
        return commodity.findByDepartment(departmentName);
    }

    @Override
    public void printCommodities(List<Commodity> commodities) {
        commodity.printCommodities(commodities);
    }

    @Override
    public void readCommodities() throws IOException {
        commodity.readCommodityFromFile();
    }

    @Override
    public void getAllCommodities() {
        commodity.getAllCommodities();
    }

    @Override
    public Commodity findByName(String name) {
        return commodity.findByName(name);
    }

    @Override
    public List<Commodity> findByPrice(Character filter, double price) {
        return commodity.findByPrice(filter, price);
    }

    @Override
    public List<Commodity> findByGroupName(String groupName) {
        return commodity.findByGroupName(groupName);
    }
}
