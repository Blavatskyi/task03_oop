package com.epam.home.view;

import com.epam.home.controller.Controller;
import com.epam.home.controller.ControllerImpl;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    public static Controller controller;
    private Map<String, String> menu;
    private static Scanner scanner = new Scanner(System.in);

    public MyView() throws IOException {
        controller = new ControllerImpl();
        controller.readCommodities();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print all Commodities");
        menu.put("2", "  2 - find commodity by Name");
        menu.put("3", "  3 - find commodities by Price");
        menu.put("4", "  4 - find commodities by Group");
        menu.put("5", "  5 - find commodities by Department");
        menu.put("Q", "  Q - exit");
    }

    public void pressButton1() {
        controller.getAllCommodities();
    }

    private void pressButton2() {
        System.out.println("Enter name of commodity you are looking for: ");
        String name = scanner.nextLine();
        if (controller.findByName(name) == null) {
            System.out.println("Commodity: " + name + " doesn't exist.");
        } else {
            System.out.println(controller.findByName(name).getName() + "--> "
                    + controller.findByName(name).getPrice());
        }
    }

    private void pressButton3() {
        System.out.println("Enter sign and price of commodities " +
                "you are looking for: ");
        String str = scanner.nextLine();
        char sign = str.charAt(0);
        double price = scanner.nextDouble();
        scanner.nextLine();

        if (controller.findByPrice(sign, price) == null) {
            System.out.println("There are no commodities " +
                    "with entered filters." +
                    "Please try one more time.");
        } else {
            controller.printCommodities(controller.findByPrice(sign, price));
        }
    }

    private void pressButton4() {
        System.out.println("Enter group of commodities you are looking for: ");
        String name = scanner.nextLine();
        if (controller.findByGroupName(name) == null) {
            System.out.println("Group: " + name + " doesn't exist.");
        } else {
            controller.printCommodities(controller.findByGroupName(name));
        }
    }

    private void pressButton5() {
        System.out.println("Enter department of commodities" +
                " you are looking for: ");
        String name = scanner.nextLine();
        if (controller.findByDepartment(name) == null) {
            System.out.println("Department: " + name + " doesn't exist.");
        } else {
            controller.printCommodities(controller.findByDepartment(name));
        }
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String str;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            str = scanner.nextLine().toUpperCase();
            char keyMenu = str.charAt(0);
            if (keyMenu == '1') {
                pressButton1();
            } else if (keyMenu == '2') {
                pressButton2();
            } else if (keyMenu == '3') {
                pressButton3();
            } else if (keyMenu == '4') {
                pressButton4();
            } else if (keyMenu == '5') {
                pressButton5();
            } else {
                System.out.println("Please, enter appropriate action.");
            }
        } while (!str.equals("Q"));
    }
}
