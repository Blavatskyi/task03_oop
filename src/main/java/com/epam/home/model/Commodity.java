package com.epam.home.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SuppressWarnings("ALL")
public class Commodity {
    private String name;
    private String department;
    private String groupName;
    private double price;

    private List<Commodity> commodities = new ArrayList<>();

    public Commodity() {
    }

    public Commodity(String name, String department, String groupName,
                     double price) {
        this.name = name;
        this.department = department;
        this.groupName = groupName;
        this.price = price;
    }

    public void readCommodityFromFile() throws IOException {
        File file = new File(
                "D:/EPAM/Topic 3. Java Basics OOP/task03_oop/commodities.txt");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String str;
        String name = "";
        String department = "";
        String groupName = "";
        double price = 0.0;
        while ((str = reader.readLine()) != null) {
            String[] array = str.split("  ");
            if (array.length == 4) {
                name = array[0];
                department = array[1];
                groupName = array[2];
                price = Double.parseDouble(array[3]);
            }
            commodities.add(new Commodity(name, department, groupName, price));
        }
    }

    private List<String> getAllDepartmentsOfCommodity() {
        List<String> departments = new ArrayList<>();
        for (int i = 0; i < commodities.size(); i++) {
            Commodity commodity = this.commodities.get(i);
            if (departments.contains(commodity.getDepartment())) {
                continue;
            } else {
                departments.add(commodity.getDepartment());
            }
        }
        return departments;
    }

    public void getAllCommodities() {
        List<String> departments = getAllDepartmentsOfCommodity();
        for (int i = 0; i < departments.size(); i++) {
            System.out.println(departments.get(i));
            for (int j = 0; j < commodities.size(); j++) {
                Commodity commodity = this.commodities.get(j);
                if (commodity.getDepartment().equals(departments.get(i))) {
                    System.out.println("\t" + commodity.getName() + " --> "
                            + commodity.getPrice() + " грн");
                }
            }

        }
    }

    public Commodity findByName(String name) {
        for (int i = 0; i < commodities.size(); i++) {
            Commodity commodity = this.commodities.get(i);
            if (commodity.getName().equals(name)) {
                return commodities.get(i);
            }
        }
        return null;
    }

    public List<Commodity> findByDepartment(String departmentName) {
        List<Commodity> commodityList = new ArrayList<>();
        for (int i = 0; i < commodities.size(); i++) {
            Commodity commodity = this.commodities.get(i);
            if (commodity.getDepartment().equals(departmentName)) {
                commodityList.add(commodity);
            }
        }
        return commodityList;
    }


    public List<Commodity> findByPrice(Character filter, double price) {
        List<Commodity> commodityList = new ArrayList<>();
        for (int i = 0; i < commodities.size(); i++) {
            Commodity commodity = this.commodities.get(i);
            if (filter == '=') {
                if (commodities.get(i).getPrice() == price) {
                    commodityList.add(commodity);
                }
            } else if (filter == '>') {
                if (commodities.get(i).getPrice() > price) {
                    commodityList.add(commodity);
                }
            } else if (filter == '<') {
                if (commodities.get(i).getPrice() < price) {
                    commodityList.add(commodity);
                }
            } else {
                System.out.println("Your filter option is not correct! " +
                        "Please, use only '=', '>' or '<'.");
                return null;
            }
        }
        return commodityList;
    }

    public List<Commodity> findByGroupName(String groupName) {
        List<Commodity> commodityList = new ArrayList<>();
        for (int i = 0; i < commodities.size(); i++) {
            Commodity commodity = this.commodities.get(i);
            if (commodity.getGroupName().equals(groupName)) {
                commodityList.add(commodity);
            }
        }
        return commodityList;
    }

    public void printCommodities(List<Commodity> commodity) {
        for (int i = 0; i < commodity.size(); i++) {
            System.out.println(commodity.get(i).getName() + " --> "
                    + commodity.get(i).getPrice());
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<Commodity> getCommodities() {
        return commodities;
    }

    public void setCommodities(List<Commodity> commodities) {
        this.commodities = commodities;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Commodity commodity = (Commodity) o;
        return Double.compare(commodity.price, price) == 0 &&
                Objects.equals(name, commodity.name) &&
                Objects.equals(department, commodity.department) &&
                Objects.equals(groupName, commodity.groupName) &&
                Objects.equals(commodities, commodity.commodities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, department, groupName, price, commodities);
    }

    @Override
    public String toString() {
        return "Commodity{" +
                "name='" + name + '\'' +
                ", department='" + department + '\'' +
                ", groupName='" + groupName + '\'' +
                ", price=" + price +
                '}';
    }
}
